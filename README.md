## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
This is simple beginner portfolio project with jobs and blog.
	
## Technologies
Project is created with:
* Python version: 3.6.8
* Django version: 2.0.2
* SQLite version: 3
	
## Setup
To run this project, install it locally (in the virtualenv) using:

```
$ cd ../portfolio-project
$ pip install -r requirements.txt
$ python manage.py makemigrations
$ python manage.py migrate
$ python manage.py createsuperuser
$ python manage.py runserver
```
